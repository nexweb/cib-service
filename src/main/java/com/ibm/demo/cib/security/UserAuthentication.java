package com.ibm.demo.cib.security;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import lombok.ToString;

@ToString
public class UserAuthentication implements Authentication {
	private String username;
	private String token;
	private Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
	UserAuthentication(String username, String token, Collection<SimpleGrantedAuthority> authorities) {
		this.username = username;
		this.token = token;
		this.authorities = authorities;
	}
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return username;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return authorities;
	}

	@Override
	public Object getCredentials() {
		// TODO Auto-generated method stub
		return token;
	}

	@Override
	public Object getDetails() {
		// TODO Auto-generated method stub
		return username;
	}

	@Override
	public Object getPrincipal() {
		// TODO Auto-generated method stub
		return username;
	}

	@Override
	public boolean isAuthenticated() {
		// TODO Auto-generated method stub
		return authorities.size() > 0;
	}

	@Override
	public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
		// TODO Auto-generated method stub

	}

}
