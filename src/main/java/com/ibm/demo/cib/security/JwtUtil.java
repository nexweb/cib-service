package com.ibm.demo.cib.security;

import java.security.SignatureException;
import java.util.Date;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.ibm.demo.cib.user.model.User;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.UnsupportedJwtException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class JwtUtil {

    public final static long TOKEN_VALIDATION_SECOND = (1000L * 60 * 60) * 24 * 6;
    public final static long REFRESH_TOKEN_VALIDATION_SECOND = 1000L * 60 * 24 * 2;

    final static public String ACCESS_TOKEN_NAME = "accessToken";
    final static public String REFRESH_TOKEN_NAME = "refreshToken";

    @Value("${bootcamp.security.key}")
    private String SECRET_KEY;
    
    public static final String AUTH_HEADER = "Authorization"; 
    public static final String TOKEN_TYPE = "Bearer";

    
    public static void main(String[] args) {
//    	JwtUtil jwtUtil = new JwtUtil();
//    	
//    	User user = new User("yun", "1234");
//    	String token = jwtUtil.generateToken(user);
//    	System.out.println("token:" + token);
//    	System.out.println("isTokenExpired:" + jwtUtil.isTokenExpired(token));
//    	System.out.println("validateToken:" + jwtUtil.validateToken(token, new CustomUserDetail(user, new ArrayList())));
//    
    }

//    private Key getSigningKey(String secretKey) {
//        byte[] keyBytes = secretKey.getBytes(StandardCharsets.UTF_8);
//        return new Key
//    }

    public Claims extractAllClaims(String token) throws ExpiredJwtException {
        return Jwts.parser()
                .setSigningKey((SECRET_KEY))
                .parseClaimsJws(token)
                .getBody();
    }

    public String getUsernameFromJWT(String token) {
        return extractAllClaims(token).get("username", String.class);
    }

    public Boolean isTokenExpired(String token) {
        final Date expiration = extractAllClaims(token).getExpiration();
        return expiration.before(new Date());
    }

    public String generateJwtToken(User member) {
        return doGenerateToken(member.getUsername(), TOKEN_VALIDATION_SECOND);
    }
    
    public String generateJwtToken(String username) {
        return doGenerateToken(username, TOKEN_VALIDATION_SECOND);
    }

    
    public String generateRefreshToken(String username) {
        return doGenerateToken(username, TOKEN_VALIDATION_SECOND);
    }
    
    public String generateRefreshToken(User member) {
        return doGenerateToken(member.getUsername(), REFRESH_TOKEN_VALIDATION_SECOND);
    }

    public String doGenerateToken(String username) {
    	return doGenerateToken(username, TOKEN_VALIDATION_SECOND);
    }
    
    public String doGenerateToken(String username, long expireTime) {

        Claims claims = Jwts.claims();
        claims.put("username", username);

        String jwt = Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + expireTime))
                .signWith(SignatureAlgorithm.HS512,SECRET_KEY)
                
                .compact();

        
        return jwt;
    }

    public Boolean validateToken(String token, UserDetails userDetails) {
        final String username = getUsernameFromJWT(token);

        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }
    
	//retrieve expiration date from jwt token
	public Date getExpirationDateFromToken(String token) {
		return getClaimFromToken(token, Claims::getExpiration);
	}

	public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
		final Claims claims = extractAllClaims(token);
		return claimsResolver.apply(claims);
	}
    
    // Jwt 토큰 유효성 검사
    public  boolean validateToken(String token) {
        try {
            Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token);
            return true;
        } catch (io.jsonwebtoken.SignatureException ex) {
            log.error("Invalid JWT signature");
        } catch (MalformedJwtException ex) {
            log.error("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            log.error("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            log.error("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            log.error("JWT claims string is empty.");
        }
        return false;
    }

}
