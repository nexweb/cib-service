package com.ibm.demo.cib.security;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.net.HttpHeaders;
import com.ibm.demo.cib.user.model.User;

import lombok.extern.slf4j.Slf4j;


@Slf4j
public class CustomUsernamePasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    
	@Autowired
	private JwtUtil jwtUtil;
	
	@Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response){
    	UsernamePasswordAuthenticationToken authToken = null;
    	
    	log.info("post request========="+request.getHeader("Content-Type"));
		if (!request.getMethod().equals("POST")) {
			log.info("post request=========");
			throw new AuthenticationServiceException("Authentication method not supported: " + request.getMethod());
		}
		
        if (request.getHeader("Content-Type").indexOf("application/json") >= 0) {
            try {
                /*
                 * HttpServletRequest can be read only once
                 */
                StringBuffer sb = new StringBuffer();
                String line = null;

                BufferedReader reader = request.getReader();
                while ((line = reader.readLine()) != null){
                    sb.append(line);
                }

                //json transformation
                ObjectMapper mapper = new ObjectMapper();
                User user = mapper.readValue(sb.toString(), User.class);
                
                log.info("request user:{}", user);
                
                authToken = new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword());
                
                log.info("user authToken:{}", authToken);
        		setDetails(request, authToken);
        		
        		return this.getAuthenticationManager().authenticate(authToken);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return authToken;
        } else {

        	String username = request.getParameter("username");
        	String password = request.getParameter("password");
        	log.info("not json request user:{}, pass:{}", username, password);
        	authToken = new UsernamePasswordAuthenticationToken(username, password);
        	
        	log.info("non json form authToken:{}", authToken);
        	setDetails(request, authToken);
        	return this.getAuthenticationManager().authenticate(authToken);
        }
    }


    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authentication) throws IOException, ServletException {
//    	super.successfulAuthentication(request, response, chain, authentication);
    	
    	
    	UserDetails user = (UserDetails)authentication.getPrincipal();
    	String access_token = jwtUtil.generateJwtToken(user.getUsername());
    			
    	String refresh_token = jwtUtil.generateRefreshToken(user.getUsername());
    	
    	response.setHeader(HttpHeaders.AUTHORIZATION,  "Bearer " + access_token);
    	response.setHeader("access_token",  access_token);

    	
    	Cookie accessToken = CookieUtil.createCookie(JwtUtil.ACCESS_TOKEN_NAME, access_token);
    	
    	response.addCookie(accessToken);          
    	
    	Map<String, String> tokens = new HashMap<>();
    	tokens.put("access_token", access_token);
    	
    	log.info("successfulAuthentication:{}", access_token);
    	
    	response.setContentType(MediaType.APPLICATION_JSON_VALUE);
    	//new ObjectMapper().writeValue(response.getOutputStream(), tokens);
    	response.sendRedirect("/");
        
    }
}