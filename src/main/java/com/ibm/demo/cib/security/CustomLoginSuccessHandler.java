package com.ibm.demo.cib.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import com.ibm.demo.cib.user.model.User;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CustomLoginSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {
	JwtUtil jwtUtil = new JwtUtil();
	@Override
	public void onAuthenticationSuccess(final HttpServletRequest request, final HttpServletResponse response,
			final Authentication authentication) {
		final User user =  (User)authentication.getPrincipal();
		final String token = jwtUtil.generateJwtToken(user);
		
		log.info("CustomLoginSuccessHandler:{}",token);
		response.addHeader(JwtUtil.AUTH_HEADER, JwtUtil.TOKEN_TYPE + " " + token);
	}
}
