package com.ibm.demo.cib.security;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CustomLogoutHandler implements LogoutHandler {

	@Override
	public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
		log.info("logout=========");
		try {
	        Cookie cookie = new Cookie(JwtUtil.ACCESS_TOKEN_NAME, "");
	        cookie.setPath("/");
	        cookie.setMaxAge(0);

	        response.addCookie(cookie);
			response.sendRedirect("/");
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

}
