package com.ibm.demo.cib.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.ibm.demo.cib.user.service.UserDetailService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	private final UserDetailService userDetailService;
	
	private final JwtAuthorizationFilter customAuthorizationFilter;
	
	private final JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;
	
    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
    
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
    
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailService).passwordEncoder(bCryptPasswordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                	.exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint)
                .and()
                	.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()	
                    .authorizeRequests()
                    .antMatchers("/", "/auth/**", "/js/**", "/css/**", "/image/**", "/api/v1/**").permitAll()
                    .anyRequest().permitAll()
                    
                .and()
                    .formLogin()
                    .disable()    
                    
     				.addFilter(authenticationFilter())

     				.addFilterBefore(customAuthorizationFilter, UsernamePasswordAuthenticationFilter.class)
     				
     				//.cors().configurationSource(corsConfigurationSource())
     			//.and()
     				.logout().addLogoutHandler(new CustomLogoutHandler());
        
        // Remember Me 구현
        // 쿠키를 얼마나 유지할 것인지 계산. (7일 설정)
        http.rememberMe().tokenValiditySeconds(60 * 60 * 7)
        	.userDetailsService(userDetailService);
    }
    
    @Bean 
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();

        configuration.addAllowedOrigin("*");
        configuration.addAllowedHeader("*");
        configuration.addAllowedMethod("*");
        configuration.setAllowCredentials(true);

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
    
	@Bean
	public CustomUsernamePasswordAuthenticationFilter authenticationFilter() throws Exception{
		CustomUsernamePasswordAuthenticationFilter authenticationFilter = new CustomUsernamePasswordAuthenticationFilter();
		authenticationFilter.setAuthenticationManager(authenticationManager());
		authenticationFilter.setFilterProcessesUrl("/auth/user/login");
		authenticationFilter.setAuthenticationSuccessHandler(customLoginSuccessHandler());

	    return authenticationFilter;
	}
	
	@Bean 
	public CustomLoginSuccessHandler customLoginSuccessHandler() { 
		return new CustomLoginSuccessHandler(); 
	}
}
