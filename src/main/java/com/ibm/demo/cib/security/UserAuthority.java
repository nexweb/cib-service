package com.ibm.demo.cib.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import lombok.ToString;

@ToString
@Component
public class UserAuthority {
	private Authentication auth;
	
	public Authentication getAuthentication() {
		auth = SecurityContextHolder.getContext().getAuthentication();
		 
		return auth;
	}
}
