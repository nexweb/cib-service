package com.ibm.demo.cib.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ibm.demo.cib.user.model.Role;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class JwtAuthorizationFilter extends OncePerRequestFilter {

	@Autowired
	private JwtUtil jwtUtil;
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		log.info("doFilterInternal request:{}", request.getServletPath());
		if (request.getServletPath().equals("/auth/user/login")) {
			filterChain.doFilter(request, response);
		} else {
			String token = getJwtFromCookie(request);
			log.info("token:{}", token);
			if (token == null) {
				token = getJwtFromRequestHeader(request);
			}
			
			
			log.info("SecurityContextHolder.Context.Authentication:{}",SecurityContextHolder.getContext().getAuthentication());
            
			if (token != null && SecurityContextHolder.getContext().getAuthentication() == null) {
				try {

					String username = jwtUtil.getUsernameFromJWT(token);
//					String[] roles = decodedJWT.getClaim("roles").asArray(String.class);
					

					Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
					authorities.add(new SimpleGrantedAuthority(Role.USER.getKey()));
					log.info("username from cookie:{}, authorities:{}", username, authorities);
					
//					Stream.of(roles).forEach(role -> {
//						authorities.add(new SimpleGrantedAuthority(Role.USER.getKey()));
//					});
					
					
					UserAuthentication userAuthentication = new UserAuthentication(username, token, authorities);
//					UsernamePasswordAuthenticationToken  authenticationToken = 
//							new UsernamePasswordAuthenticationToken(username, token, authorities);
					SecurityContextHolder.getContext().setAuthentication(userAuthentication);
					log.info("userAuthentication:{}", userAuthentication);
					filterChain.doFilter(request, response);
				} catch (Exception e) {
					log.error("error:{}", e);
					response.setHeader("error", e.getMessage());
					response.setStatus(HttpStatus.FORBIDDEN.value());
					Map<String, String> error = new HashMap<>();
					error.put("error_message", e.getMessage());
					response.setContentType(MediaType.APPLICATION_JSON_VALUE);
					new ObjectMapper().writeValue(response.getOutputStream(), error);
				}

			} else {
				filterChain.doFilter(request, response);
			}
				
		}

	}
	
	/**
	 * Cookie 로 부터 token 추출 
	 * @param request
	 * @return
	 */
    private String getJwtFromCookie(HttpServletRequest request) {
    	Cookie jwtToken = CookieUtil.getCookie(request,JwtUtil.ACCESS_TOKEN_NAME);
		String token = null;
		
        if(jwtToken != null){                 
        	token = jwtToken.getValue();                 
        	log.info("jwt from cookie:{}", token);            
        }
        
        return token;
    }

    /**
     * Header로 부터 token 추출 
     * @param request
     * @return
     */
    private String getJwtFromRequestHeader(HttpServletRequest request) {
        String bearerToken = request.getHeader(JwtUtil.AUTH_HEADER);
        StringBuilder sb = new StringBuilder();
        sb.append(JwtUtil.TOKEN_TYPE).append(" ");
        
        if (bearerToken != null && bearerToken.startsWith(sb.toString())) {
            return bearerToken.substring(sb.length());
        }
        return null;
    }
}
