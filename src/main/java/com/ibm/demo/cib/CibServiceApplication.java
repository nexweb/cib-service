package com.ibm.demo.cib;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CibServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CibServiceApplication.class, args);
	}

}
