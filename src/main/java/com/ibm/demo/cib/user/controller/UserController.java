package com.ibm.demo.cib.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ibm.demo.cib.security.UserAuthority;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class UserController {

	@Autowired
	private UserAuthority userAuthority;
    /**
     * 회원수정 페이지
     */
    @GetMapping("/cib/sample")
    public String getHello() {
    	
        return "Hello Sample";
    }  
    
    @PostMapping("/cib/sample")
    public String saveHello() {
    	String username  = "No Authentication ";
    	try {
    		log.info("call sample");
        	Authentication auth = userAuthority.getAuthentication();
        	log.info("user:{}", userAuthority.getAuthentication());
        	username =  auth.getName();
    	} catch(Exception e) {
    		e.printStackTrace();
    	}

        return username;
    }      
}
