package com.ibm.demo.cib.user.api;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ibm.demo.cib.user.controller.dto.UserDto;
import com.ibm.demo.cib.user.model.PrincipalDetail;
import com.ibm.demo.cib.user.model.User;
import com.ibm.demo.cib.user.service.UserService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@Slf4j
@RestController
public class UserApiController {
	
	private final UserService userService;
	
    @PostMapping("/auth/api/v1/user")
    public Long save(@RequestBody UserDto userSaveRequestDto) {
    	log.info("=================================");
    	log.info("userSaveRequestDto:{}", userSaveRequestDto);
    	
    	User user = userSaveRequestDto.toEntity();

    	userService.save(user);
    	
        return 1L;
    }
    
    /**
     * 회원수정 API
     */
    @PutMapping("/api/v1/user")
    public Long update(@RequestBody UserDto userSaveRequestDto, @AuthenticationPrincipal PrincipalDetail principalDetail) {
    	User user = userSaveRequestDto.toEntity();
    	
    	log.info("update:{}", principalDetail);
        return userService.update(user);
    }   
}
