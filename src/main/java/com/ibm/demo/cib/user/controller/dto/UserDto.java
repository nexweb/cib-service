package com.ibm.demo.cib.user.controller.dto;


import com.ibm.demo.cib.user.model.Role;
import com.ibm.demo.cib.user.model.User;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UserDto {
    private String username;
    private String password;
    private String nickname;
    
    public User toEntity() {
    	return new User(username, password, nickname, Role.USER.getKey());
    }
}
